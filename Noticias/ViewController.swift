//
//  ViewController.swift
//  Noticias
//
//  Created by Bootcamp 2 on 2022-11-22.
//

import UIKit
import SafariServices

// MARK: se crea el objeto de tipo articulo
struct NoticiasModelo: Codable {
    var articles: [Noticia]
}

// MARK: Struct de los campos que vi=oy a mostrar en la celda
struct Noticia: Codable {
    let title: String?
    let description: String?
    let url: String?
    let urlToImage: String?
    
}


// MARK: view controller normal
class ViewController: UIViewController {
    
    // instancia del objeto NOTICIA
    var articuloNoticia: [Noticia] = []
    // uitableviewcell
    @IBOutlet weak var tablaNoticias: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tablaNoticias.register(UINib(nibName: "CeldaNoticiaTableViewCell", bundle: nil), forCellReuseIdentifier: "celdaNoticia")
        
        tablaNoticias.delegate = self
        tablaNoticias.dataSource = self
        
        
        //MARK: Se llama a la funcion que hace la consulta a la API
        buscarNoticias()
        
    }
    // MARK: Funcion que hace la consulta a la APi
    func buscarNoticias() {
        
        let urlString = "https://newsapi.org/v2/top-headlines?apiKey=f0797ef3b62d4b90a400ed224e0f82b7&country=mx"
        
        if let url = URL(string: urlString){
            
            if let data = try? Data(contentsOf: url){
                let decodificador = JSONDecoder()
                
                if let datosDecodificados = try? decodificador.decode(NoticiasModelo.self, from: data) {
                    print("datos decodificados: \(datosDecodificados.articles)")
                    
                    articuloNoticia = datosDecodificados.articles
                    
                    tablaNoticias.reloadData()
                }
            }
        }
        
    }
    
}


// MARK: Delegates necesarios para la tableview
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articuloNoticia.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = tablaNoticias.dequeueReusableCell(withIdentifier: "celdaNoticia", for: indexPath) as! CeldaNoticiaTableViewCell
        
        celda.tituloNoticia.text = articuloNoticia[indexPath.row].title
        celda.descriptionNoticia.text = articuloNoticia[indexPath.row].description
        
        if let url = URL(string: articuloNoticia[indexPath.row].urlToImage ?? ""){
            if let imagenData = try? Data(contentsOf: url){
                celda.imagenNoticia.image = UIImage(data: imagenData)
            }
        }
        
        return celda
        
    }
    
    //MARK: Cuando se selecciona una noticia
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        tablaNoticias.deselectRow(at: indexPath, animated: true)
        
        //MARK: URL a mostrar en caso de que el usuario haya seleccionado alguna noticia/celda
        
        guard let urlMostrar = URL(string: articuloNoticia[indexPath.row].url ?? "") else { return }
        
        // MARK: ViewController de tipo SAFARISERVICES nuevo para mostrar la noticia que se haya seleccionado en el modo navegador
        
        let viewControllerSS = SFSafariViewController(url: urlMostrar)
        
        present(viewControllerSS, animated: true)
    }
}

