//
//  CeldaNoticiaTableViewCell.swift
//  Noticias
//
//  Created by Bootcamp 2 on 2022-11-22.
//

import UIKit

class CeldaNoticiaTableViewCell: UITableViewCell {

    @IBOutlet weak var tituloNoticia: UILabel!
    @IBOutlet weak var descriptionNoticia: UILabel!
    
    @IBOutlet weak var imagenNoticia: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
